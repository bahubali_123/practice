const todosData = [
    {
        id: 1,
        text: "Bangalore",
        completed: false
    },
    {
        id: 2,
        text: "Mangalore",
        completed: false
    },
    {
        id: 3,
        text: "Delhi",
        completed: false
    },
    {
        id: 4,
        text: "Mumbai",
        completed: false
    },
    {
        id: 5,
        text: "Hydrabad",
        completed: false
    }
]

export default todosData