import React, {Component} from "react"

class FormData extends React.Component {

    constructor() {
        super()
        this.state = {
            firstName:"",
            lastName:""
        }
        this.handleClick = this.handleClick.bind(this)
    }

    handleClick (event) {
        const {name, value} = event.target
        this.setState({
            [name] : value
        })

    }

    render() {
        return (
            <form>
              <input type="text" name ="firstName"
              onChange={this.handleClick}
              placeholder="First Name" /> <br/>

              <input type="text" name ="lastName"
              onChange={this.handleClick}
              placeholder="Last Name" />

              <h2>{this.state.firstName} {this.state.lastName}</h2>               
                
            </form>
        )
    }

}

export default FormData