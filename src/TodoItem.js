import React from "react"

function TodoItem(props) {

    const notCompletedStyle = {
        fontStyle: "italic",
        fontSize:20,
        color: "black",
    }

    const completedStyle = {
        fontStyle: "italic",
        color: "red",
        fontSize:20,
        textDecoration: "line-through"
    }
    
    return (
        <div className="todo-item">
            <input 
                type="checkbox" 
                checked={props.item.completed} 
                onChange={() => props.handleChange(props.item.id)}
            />
            <p style={props.item.completed ? completedStyle: notCompletedStyle}>{props.item.text}</p>
        </div>
    )
}

export default TodoItem